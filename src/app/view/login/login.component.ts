import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  title = 'guia';
  hide = true;
  email = new FormControl('', [Validators.required, Validators.email]);

  constructor(private router: Router) {

  }

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' :
            '';
  }

  entrar(){
    this.router.navigate(['cidade']);
  }


}
