import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { DialogOverviewExampleDialog } from '../shared/dialog/dialog.component';

@Component({
  selector: 'app-overviewCidade',
  templateUrl: './overviewCidade.component.html',
  styleUrls: ['./overviewCidade.component.css']
})
export class OverviewCidadeComponent {

  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    let dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '250px',



    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

    });
  }

}


