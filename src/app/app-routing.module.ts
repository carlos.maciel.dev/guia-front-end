import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { BuscarCidade } from './view/buscarCidade/buscarCidade.component';
import { LoginComponent } from './view/login/login.component';
import { OverviewCidadeComponent } from './view/overviewCidade/overviewCidade.component';

const routes: Routes = [

  { path: '', component: LoginComponent },
  { path: 'buscarCidade', component: BuscarCidade },
  {path: 'cidade', component: OverviewCidadeComponent}



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
